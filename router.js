const { Router } = require('express')
const router = Router()

// Controller import
const Controller = require('./controller')

router.route('/')
  .post(Controller.createTodo)
  .get(Controller.getAllTodos)

router.route('/:id')
  .get(Controller.getTodoWithId)
  .put(Controller.updateTodoWithId)
  .delete(Controller.deleteTodoWithId)


module.exports = router