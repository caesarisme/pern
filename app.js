const express = require('express')
const pool = require('./db')

const app = express()

// Middleware
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

// Routes
app.use('/api/todos', require('./router'))

const PORT = 3000
app.listen(PORT, () => console.log(`Listening port ${ PORT }`))