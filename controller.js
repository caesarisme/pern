const pool = require('./db')

module.exports = {
  createTodo: async (req, res) => {
    try {
      const { description } = req.body
      const newTodo = await pool.query(
        'INSERT INTO todo(description) VALUES($1) RETURNING *',
        [description]
      )

      res.status(201).json(newTodo.rows[0])
    } catch (e) {
      console.log(e.message)
    }
  },

  getAllTodos: async (req, res) => {
    try {
      const todos = await pool.query('SELECT * FROM todo')

      res.status(200).json(todos.rows)
    } catch (e) {
      console.log(e.message)
    }
  },

  getTodoWithId: async (req, res) => {
    try {
      const { id } = req.params
      const todo = await pool.query('SELECT * FROM todo WHERE id = $1', [id])

      res.status(200).json(todo.rows[0])
    } catch (e) {
      console.log(e.message)
    }
  },

  updateTodoWithId: async (req, res) => {
    try {
      const { id } = req.params
      const { description } = req.body

      const newTodo = await pool.query(
        'UPDATE todo SET description=$2 WHERE id=$1 RETURNING *',
        [id, description]
      )

      res.status(200).json(newTodo.rows[0])
    } catch (e) {
      console.log(e.message)
    }
  },

  deleteTodoWithId: async (req, res) => {
    try {
      const { id } = req.params

      await pool.query('DELETE FROM todo WHERE id=$1', [id])

      res.sendStatus(200)
    } catch (e) {
      console.log(e.message)
    }
  }
}